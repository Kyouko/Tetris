#include "queue.h"

void queueAddElement(const queue restrict queue,
                     const void *const restrict value, size_t position) {
    queueElement *current = (queueElement *) queue;
    queueElement *next;
    while (position != 0 && current->next) {
        current = current->next;
    }
    next = queueNewElement(value);
    next->next = current->next;
    current->next = next;
}

/* void *queueAt(queue queue, size_t position); */
/* void queueDeleteElement(queue queue, size_t position); */
/* void queueDeleteLastElement(queue queue); */
/* inline char queueIsLast(queueElement *current); */
/* queueElement *queueNewElement(void); */
/* inline queueElement *queueNext(queueElement *current); */
