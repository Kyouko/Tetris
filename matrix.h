#include <stdbool.h>
#include "tetromino.h"


typedef struct matrix {
    unsigned char width, height;
    enum tetrominoType **matrix;
} matrix;

matrix createMatrix();
void clearMatrix(matrix *field);
void clearLine(matrix *field, unsigned char line);
bool doesPieceCollide(matrix *field, tetromino *piece,
                      unsigned char x, unsigned char y);
void dropPiecePosition(matrix *field, tetromino *piece,
                       unsigned char x, unsigned char y);
void placePiece(matrix *field, tetromino *piece,
                unsigned char x, unsigned char y);
