#include <stdlib.h>

/* Queue implemented as a singly linked list */

typedef  struct queueElement {
    struct queueElement *next;
    void *value;
} queueElement;

typedef struct queue {
        queueElement *first;
        size_t size;
} *queue;

#define _INIT_QUEUE {NULL, 0}


void queueAddElement(const queue restrict queue,
                     const void *const restrict value, size_t position);
void queueDeleteElement(queue queue, size_t position);
void queueDeleteLastElement(queue queue);
inline char queueIsLast(const queueElement *const current);
queueElement *queueNewElement(const void *const value);
