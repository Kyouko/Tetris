typedef struct tetromino {
    enum tetrominoType **piece;
    unsigned char size;
} tetromino;

enum tetrominoType {
    I_PIECE, J_PIECE, L_PIECE, O_PIECE, S_PIECE, T_PIECE, Z_PIECE,
    NO_PIECE
};

enum rotationDirection {
    CLOCKWISE, CTCLOCKWISE
};

tetromino makeTetrominoRandom();
tetromino makeTetrominoFromPiece();
void rotateTetromino(tetromino *piece, enum rotationDirection direction);
